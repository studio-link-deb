DOWNLOAD_BASEURL := "https://download.studio.link/releases"
VERSION := 21.07.0

all: studio-link-standalone studio-link.lv2 #studio-link-onair.lv2

studio-link-standalone: v$(VERSION)/studio-link-standalone-v$(VERSION).tar.gz
	@echo "extracting files... from $<"
	@tar -xzf $<
	@mv studio-link-standalone-v$(VERSION) $@

studio-link.lv2: v$(VERSION)/studio-link-plugin.zip
	@echo "extracting files... from $<"
	@unzip -u $<

# studio-link-onair.lv2: v$(VERSION)/studio-link-plugin-onair.zip
# 	@echo "extracting files... from $<"
# 	@unzip -u $<

v$(VERSION)/%:
	@echo "fetching file: $(DOWNLOAD_BASEURL)/v$(VERSION)-stable/linux/$(notdir $@)"
	@curl -L -f -s -S "$(DOWNLOAD_BASEURL)/v$(VERSION)-stable/linux/$(notdir $@)" > "$@"
	@cd v$(VERSION) && sha256sum -c "$(notdir $@).sha256"

clean:
	rm -f v$(VERSION)/*.tar.gz
	rm -f v$(VERSION)/*.zip
	rm -f studio-link-standalone
	rm -rf studio-link.lv2
	rm -rf studio-link-onair.lv2
